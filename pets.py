# ---------------------------------------------------------------------------
# Pets

pets = (
    ('Achievements', (
        63724, # Elayna (Glory of the Tomb Raider)
        64632, # Hopling - Ling-Ting's Herbal Journey
        32643, # Kirin Tor Familiar - Higher Learning
        112167,# Lagan (Poor Unfortunate Souls, Maw of Souls)
        45247, # Pebble - Rock Lover
        37865, # Perky Pug - Looking For Multitudes
        107206,# Zoom (Zoom!, Stonedark Grotto)
    )),
    ('Achievements - Pets', (
        23274, # Stinker - Shop Smart, Shop Pet...Smart (50 pets)
        32939, # Little Fawn - Lil' Game Hunter (75 pets)
        54227, # Nuts - Petting Zoo (100 pets)
        54374, # Brilliant Kaliri - Menagerie (125 pets)
        40624, # Celestial Dragon - Littlest Pet Shop (150 pets)
        63621, # Feral Vermling - Going to Need More Leashes (250 pets)
        66491, # Venus - That's a Lot of Pet Food (400 pets)
        87705, # Stormwing - So. Many. Pets. (600 pets)
        66450, # Jade Tentacle (400 pet achievement points)
        64232, # Singing Cricket - Pro Pet Mob (75 pets to 25)
        69849, # Stunted Direhorn - Brutal Pet Brawler
        80101, # Royal Peachick - Draenor Safari
        88830, # Trunks - An Awfully Big Adventure
        113855,# Rescued Fawn (Legion Safari)
        112945,# Nightmare Treant (Legion meta)
        128146,# Felclaw Marsuul (Achievement, Family Fighter)
        68655, # Mr. Bigglesworth - Raiding With Leashes
        71023, # Tito - Raiding With Leashes II
        90215, # K'ute - Raiding With Leashes III
        127956,# Amalgam of Destruction (Achievement, Raiding with Leashes V: Cataclysm)
    )),
    ('Garrison', (
        93143, # Cinder Pup - Mission
        79039, # Crazy Carrot - Herb Garden
        94927, # Crusher - Vendor
        88222, # Everbloom Peachick - Inn
        94867, # Left Shark - Mission
        87257, # Pygmy Cow - Barn
    )),
    ('Garrison - Pet Menagerie', (
        77021, # Albino Chimaeraling
        87704, # Firewing
        83817, # Ghastly Kid
        88300, # Puddle Terror
        83588, # Sun Sproutling
        88367, # Sunfire Kaliri
        78421, # Weebomination
        88577, # Bone Serpent
        91407, # Slithershock Elver
        91408, # Young Talbuk
        88514, # Bloodthorn Hatchling - Vendor
        88415, # Dusty Sporewing - Vendor
        88575, # Glowing Sporebat - Vendor
        93142, # Lost Netherpup - Vendor
    )),
    ('Order Hall', (
        112144,# Corgnelius
        112132,# Firebat Pup
        97178, # Hateful Eye
        119498,# Bloodbrood Whelpling (Order Hall, Death Knight)
        119499,# Frostbrood Whelpling (Order Hall, Death Knight)
        119500,# Vilebrood Whelpling (Order Hall, Death Knight)
        98463, # Broot (Order Hall, Druid)
        112798,# Nightmare Lasher (Order Hall, Druid)
        120830,# Ban-Fu, Cub of Ban-Lu (Order Hall, Monk)
        116871,# Crackers (Order Hall, Rogue?)
    )),
    ('Promotion', (
        103159,# Baby Winston (Overwatch)
        36871, # Core Hound Pup (Authenticator)
        106283,# Corgi Pup (12th Anniversary)
        33578, # Murkimus the Gladiator (Arena Tournament)
        91226, # Graves (Heroes of the Storm)
        25146, # Golden Pig (RAF)
        34930, # Jade Tiger (RAF)
        25147, # Silver Pig (RAF)
        36910, # Zipao Tiger (RAF)
    )),
    ('PvP', (
        111202,# Alliance Enthusiast (Prestige)
        111296,# Horde Fanatic (Prestige)
        115919,# Dutiful Gruntling (Prestige)
        115918,# Dutiful Squire (Prestige)
    )),
    ('Quests', (
        54539, # Alliance Balloon
        42177, # Blue Mini Jouster
        62829, # Fishy
        42183, # Gold Mini Jouster
        54541, # Horde Balloon
        86447, # Ikky
        77221, # Iron Starlette
        52894, # Lashtail Hatchling
        84330, # Meadowstomper Calf - Nagrand (WoD)
         8376, # Mechanical Chicken
        22445, # Miniwing
        52226, # Panther Cub
        69649, # Red Panda
        88225, # Sentinel's Companion
        51090, # Singing Sunflower
        69848, # Spectral Porcupette
        51632, # Tiny Flamefly
        30379, # Westfall Chicken
        34278, # Withers
    )),
    ('Quests - Daily', (
        69892, # Mountain Panda - MoP Trainer Daily
        69893, # Snowy Panda - MoP Trainer Daily
        69891, # Sunfur Panda - MoP Trainer Daily
        68467, # Pandaren Air Spirit - MoP Spirit Daily
        68468, # Pandaren Earth Spirit - MoP Spirit Daily
        68466, # Pandaren Fire Spirit - MoP Spirit Daily
        66950, # Pandaren Water Spirit - MoP Spirit Daily
        61086, # Porcupette
        93483, # Nightmare Bell
        93352, # Periwinkle Calf
        86718, # Seaborne Spore
        86715, # Zangar Spore
        73011, # Lil' Bling - Blingtron
        85284, # Sky-Bo - Blingtron 5000
        99505, # Knockoff Blingtron (Blingtron 6000)
    )),
    ('Quests - Legion', (
        96649, # Ashmaw Cub ()
        97207, # Emmigosa ()
        96622, # Grumpy ()
        111421,# Lurking Owl Kitten ()
        97079, # Skyhorn Nestling ()
        111423,# Untethered Wyrmling ()
        113827,# Wonderous Wisdomball ()
        115787,# Bloodgazer Hatchling (Quest)
        115785,# Direbeak Hatchling (Quest)
        115786,# Sharptalon Hatchling (Quest)
        115784,# Snowfeather Hatchling (Quest)
    )),
    ('Trainer', (
        63098, # Gilnean Raven - Worgen
        65314, # Jade Crane Chick - Pandaren
        63097, # Shore Crawler - Goblin
    )),
    ('Treasure', (
        73534, # Azure Crane Chick - Timeless Isle
        73668, # Bonkers - Timeless Isle
        86716, # Crimson Spore - Gorgrond
        35396, # Darting Hatchling - Dustwallow Marsh
        85387, # Fruit Hunter - Frostfire Ridge
        86879, # Hydraling - Spires of Arak
        35387, # Leaping Hatchling - The Barrens
        99526, # Leyline Broodling ()
        88814, # Nethaera's Light - Dalaran
        35397, # Ravasaur Hatchling - Un'goro Crater
        35398, # Razormaw Hatchling - Wetlands
        120397,# Scraps (Curious Wyrmtongue Cache, Broken Shore)
        25706, # Searing Scorchling
        95572, # Shard of Cyrukh - Tanaan
        85231, # Stonegrinder - Talador
        61087, # Sun Darter Hatchling (puzzle?)
        81431, # Teroclaw Hatchling - Talador
        63365, # Terrible Turnip - MoP Farm
        99389, # Thistleleaf Adventurer ()
        86717, # Umbrafen Spore - Shadowmoon Valley
    )),
    ('Vendor', (
         7394, # Ancona Chicken - Thousand Needles
        106270,# Celestial Calf ()
         7390, # Cockatiel - Cape of Stranglethorn
        53661, # Crimson Lasher - Molten Front
        128118,# Cross Gazer (Quest, A Pile of Intact Demon Eyes, Antoran Wastes - eye vendor?)
        94927, # Crusher - Garrison
        106278,# Felbat Pup (Nethershard)
        53658, # Hyjal Bear Cub - Molten Front
        118060,# Infinite Hatchling (Timeless Isle - Mistweaver Xia)
        88574, # Kaliri Hatchling - Spires of Arak
        118063,# Paradox Spirit (Timeless Isle - Mistweaver Xia)
        119040,# Tylarr Gronnden (Deeprun Tram/Brawl'gar Arena)
        88573, # Veilwatcher Hatchling - Spires of Arak
        52831, # Winterspring Cub - Winterspring
    )),
    ('Vendor - Alliance', (
         7385, # Bombay Cat - Elwynn Forest
         7384, # Cornish Rex Cat - Elwynn Forest
         7382, # Orange Tabby Cat - Elwynn Forest
         7381, # Silver Tabby Cat - Elwynn Forest
         7386, # White Kitten - Stormwind City
        21010, # Blue Moth - Exodar
        21009, # Red Moth - Exodar
        21018, # White Moth - Exodar
        21008, # Yellow Moth - Exodar
         7553, # Great Horned Owl - Teldrassil
         7555, # Hawk Owl - Teldrassil
         7560, # Snowshoe Rabbit - Dun Morogh
    )),
    ('Vendor - Dalaran', (
         7561, # Albino Snake - Dalaran
        99425, # Alarm-o-Bot (vendor Dalaran)
        83584, # Autumnal Sproutling (Dalaran - Draemus)
        40295, # Blue Clockwork Rocket Bot - Dalaran
        34364, # Calico Cat - Dalaran
        98185, # Fel Piglet (vendor dalaran)
        29147, # Ghostly Skull - Dalaran
        97127, # Nightwatch Swooper (vendor tokens)
        97238, # Nursery Spider (Dalaran)
        35399, # Obsidian Hatchling - Dalaran
        98132, # Plump Jelly (vendor dalaran)
        98128, # Sewer-Pipe Jelly (vendor dalaran)
        117371,# Trashy (Dalaran - Conjurer Margoss)
    )),
    ('Vendor - Guild', (
        49587, # Guild Herald
        49590, # Guild Herald
        49586, # Guild Page
        49588, # Guild Page
        48242, # Armadillo Pup - Critter Kill Squad
        47944, # Dark Phoenix Hatchling - United Nations
        84521, # Deathwatch Hatchling - Challenge Warlords: Gold - Guild Edition
        54027, # Lil' Tarecgosa - Dragonwrath, Tarecgosa's Rest - Guild Edition
        65313, # Thundering Serpent Hatchling - Challenge Conquerors: Gold - Guild Edition ?
    )),
    ('Vendor - Horde', (
         7565, # Black Kingsnake - Orgrimmar
         7562, # Brown Snake - Orgrimmar
         7567, # Crimson Snake - Orgrimmar
        21056, # Blue Dragonhawk - Silvermoon
        21055, # Golden Dragonhawk - Silvermoon
        21064, # Red Dragonhawk - Silvermoon
        21063, # Silver Dragonhawk - Silvermoon
        14421, # Brown Prairie Dog - Mulgore
         7395, # Undercity Cockroach - Undercity
    )),
    ('Vendor - Netherstorm', (
        20472, # Brown Rabbit - Netherstorm
         7567, # Crimson Snake - Netherstorm
        20408, # Mana Wyrmling - Netherstorm
        21009, # Red Moth - Netherstorm
         7389, # Senegal - Cape of Stranglethorn/Netherstorm
         7380, # Siamese Cat - Netherstorm
         7395, # Undercity Cockroach - Netherstorm
    )),
    ('Vendor - Timeless Isle', (
        72462, # Chi-Chi, Hatchling of Chi-Ji
        71942, # Xu-Fu, Cub of Xuen
        72463, # Yu'la, Broodling of Yu'lon
        72464, # Zao, Calfling of Niuzao
        73809, # Sky Lantern - Timeless Isle
        73688, # Vengeful Porcupette - Timeless Isle
    )),
    ('World Drops', (
         7547, # Azure Whelpling
         7546, # Bronze Whelpling
         7544, # Crimson Whelpling
         7543, # Dark Whelpling
         7545, # Emerald Whelpling
         7383, # Black Tabby Cat - Hillsbrad Foothills
        35395, # Deviate Hatchling
        15429, # Disgusting Oozeling
        71201, # Filthling - Isle of Thunder
        21076, # Firefly
        48641, # Fox Kit - Tol Barad
        71159, # Gahz'rooki - Northern Barrens
        35400, # Gundrak Hatchling
         7391, # Hyacinth Macaw - Stranglethorn
        35394, # Razzashi Hatchling
         9662, # Sprite Darter
        83562, # Zomstrok - Shadowmoon Valley (WoD)
    )),
    ('Misc', (
        124594,# Dig Rat (Pickpocket)
        124389,# Sneaky Marmot (Pickpocket)
    )),
    ('Unknown', (
        116080,# Albino Buzzard (pet supplies?)
        120591,# Arne's Test Pet (PH)
        98182, # Black Piglet (??)
        98181, # Brown Piglet (??)
        105355,# Essence of Mana (??)
        105353,# Font of Mana (??)
        120973,# Golden Retriever
        117343,# Hearthy (??)
        99874, # Mini Arcanotron (??)
        99876, # Mini Electron (??)
        99856, # Mini Magmatron (??)
        117341,# Mini Naxxy (PH)
        117340,# Mini Spider Tank (PH)
        98572, # Rocko (??)
        83592, # Sassy Sproutling (unknown)
        105356,# Seed of Mana (??)
        114063,# Snowfang (??)
        98004, # Son of Goredome (??)
    )),
    ('Unavailable', (
        32841, # Baby Blizzard Bear - WoW's 4th Anniversary
        36607, # Onyxian Whelpling - WoW's 5th Anniversary
        84915, # Molten Corgi - WoW's 10th Anniversary
        87669, # Hatespark the Tiny - Molten Core
        27346, # Essence of Competition - ? 2008
        16069, # Gurky - EU Fansite Thing
        23198, # Lucky - World Wide Invitational 2007
        29089, # Mini Tyrael - World Wide Invitational 2008
        29726, # Mr. Chilly - Battle.net Account Merger
        15361, # Murki - Korea something
        35468, # Onyx Panther - Korea something
        16456, # Poley - iCoke
        68502, # Spectral Cub - Battle.net World Championship Shanghai 2012
        27217, # Spirit of Competition - Beijing Olympics 2008
        16445, # Terky - iCoke
        14755, # Tiny Green Dragon - iCoke
        14756, # Tiny Red Dragon - iCoke
        28513, # Vampiric Batling - Karazhan event thing
        34587, # Warbot - Mountain Dew
    )),

    ('#Instances', ()),
    ('Challenge Dungeons', (
        117182,# Cavern Moccasin (Wailing Caverns)
        117180,# Everliving Spore (Wailing Caverns)
        121317,# Son of Skum (Wailing Caverns)
        117184,# Young Venomfang (Wailing Caverns)
        122629,# Foe Reaper 0.9 (Deadmines)
        124589,# Mining Monkey (Deadmines)
        119794,# Pocket Cannon (Deadmines)
        122612,# Tricorne (Deadmines)
    )),
    ('Vanilla', (
        68666, # Ashstone Core - MC
        68664, # Corefire Imp - MC
        68665, # Harbinger of Flame - MC
        68662, # Chrominius - BWL
        68663, # Death Talon - BWL
        68661, # Untamed Hatchling - BWL
        68659, # Anubisath Idol - AQ40
        68658, # Mini Mindslayer - AQ40
        68660, # Viscidus Globule - AQ40
         7387, # Green Wing Macaw - Deadmines
        10598, # Smolderdweb Hatchling - LBRS
        10259, # Worg Pup - LBRS
    )),
    ('WotLK', (
        68657, # Fungal Abomination - Naxx
        68656, # Giant Bone Spider - Naxx
        68654, # Stitched Pup - Naxx
    )),
    ('TBC', (
        71033, # Fiendish Imp - Karazhan
        71014, # Lil' Bad Wolf - Karazhan
        71015, # Menagerie Custodian - Karazhan
        71016, # Netherspace Abyssal - Karazhan
        71019, # Coilfang Stalker - SSC
        71018, # Tainted Waveling - SSC
        71017, # Tideskipper - SSC
        71021, # Lesser Voidcaller - Tempest Keep
        71022, # Phoenix Hawk Hatchling - Tempest Keep
        71020, # Pocket Reaver - Tempest Keep
        90200, # Grotesque - Hyjal Summit
        90208, # Hyjal Wisp - Hyjal Summit
        90207, # Stinkrot - Hyjal Summit
        90202, # Abyssius - Black Temple
        90203, # Fragment of Anger - Black Temple
        90205, # Fragment of Desire - Black Temple
        90204, # Fragment of Suffering - Black Temple
        90201, # Leviathan Hatchling - Black Temple
        90206, # Sister of Temptation - Black Temple
        90213, # Chaos Pup - Sunwell
        90212, # Sunblade Micro-Defender - Sunwell
        90214, # Wretched Servant - Sunwell
        26119, # Phoenix Hatchling - Magisters' Terrace
        24480, # Mojo - Zul'Aman
    )),
    ('WotLK', (
        115145,# Creeping Tentacle (Ulduar)
        115144,# G0-R41-0N Ultratonk (Ulduar)
        115139,# Ironbound Proto-Whelp (Ulduar)
        115138,# Magma Rageling (Ulduar)
        115140,# Runeforged Servitor (Ulduar)
        115141,# Sanctum Cub (Ulduar)
        115143,# Snaplasher (Ulduar)
        115142,# Winter Rageling (Ulduar)
        115135,# Dreadmaw (Trial of the Crusader)
        115137,# Nerubian Swarmer (Trial of the Crusader)
        115136,# Snobold Runt (Trial of the Crusader)
        115146,# Boneshard (Icecrown Citadel)
        115148,# Blightbreath (Icecrown Citadel)
        115147,# Blood Boil (Icecrown Citadel)
        115150,# Drudge Ghoul (Icecrown Citadel)
        115149,# Soulbroken Whelpling (Icecrown Citadel)
        115152,# Wicked Soul (Icecrown Citadel)
        115158,# Stardust (??)
    )),
    ('Cata', (
        127858, # Bound Stream (Elementium Monstrosity, Bastion of Twilight)
        127859, # Faceless Minion (Cho'gall, Bastion of Twilight)
        127857, # Twilght Clutch-Sister (Valiona, Bastion of Twilight)
        127852, # Discarded Experiment (Maloriak, Blackwing Descent)
        127853, # Rattlejaw (Nefarian, Blackwing Descent)
        127850, # Tinytron (Magmatron, Blackwing Descent)
        127953, # Corrupted Blood (Spine of Deathwing, Dragon Soul)
        127952, # Faceless Mindlasher (Yor'sahj the Unsleeping, Dragon Soul)
        127954, # Unstable Tendril (Deathwing, Dragon Soul)
        127947, # Blazehound (Shannox, Firelands)
        127948, # Cinderweb Recluse (Beth'tilac, Firelands)
        127951, # Infernal Pyreclaw (Majordomo Staghelm, Firelands)
        127950, # Surger (Baleroc, Firelands)
        127863, # Drafty (Nezir, Throne of the Four Winds)
        127862, # Zephyrian Prince (Al'Akir, Throne of the Four Winds)
    )),
    ('MoP', (
        70144, # Ji-Kun Hatchling - Throne of Thunder
        71199, # Living Fluid - Throne of Thunder
        69748, # Living Sandling - Throne of Thunder
        70083, # Pygmy Direhorn - Throne of Thunder
        69820, # Son of Animus - Throne of Thunder
        71200, # Viscous Horror - Throne of Thunder
        73352, # Blackfuse Bombling - Siege of Orgrimmar
        73350, # Droplet of Y'Shaarj - Siege of Orgrimmar
        73351, # Gooey Sha-ling - Siege of Orgrimmar
        73354, # Kovok - Siege of Orgrimmar
    )),
    ('WoD', (
        94623, # Corrupted Nest Guardian - Hellfire Citadel
        86532, # Lanticore Spawnling - UBRS
    )),
    ('Legion', (
        124944,# Ageless Bronze Drake (Chromie scenario)
        124858,# Bronze Proto-Whelp (Chromie scenario)
        97206, # Dream Whelpling (Emerald Nightmare)
        112015,# Nightmare Whelpling (Emerald Nightmare)
    )),

    # Professions
    ('#Profession', ()),
    ('Other', (
        84853, # Soul of the Forge - Blacksmithing
        78895, # Lil' Leftovers - Cooking
        128137, # Fel Lasher (Herbalism)
        83594, # Nightshade Sproutling - Herbalism (Garrison?)
        66104, # Chi-Ji Kite - Inscription
        66105, # Yu'lon Kite - Inscription
        61877, # Jade Owl - Jewelcrafting
        61883, # Sapphire Cub - Jewelcrafting
        50722, # Elementium Geode - Mining
        85667, # Ore Eater - Mining (Garrison?)
        128160, # Fossorial Bile Larva (Skinning)
    )),
    ('Alchemy', (
        98172, # Ridgeback Piglet (Alchemy)
        98183, # Thaumaturgical Piglet (Alchemy)
        111425,# Transmutant (Alchemy)
    )),
    ('Archaeology', (
        86420, # Ancient Nest Guardian
        48609, # Clockwork Gnome
        45128, # Crawling Claw
        45340, # Fossilized Hatchling
        86422, # Frostwolf Ghostpup
        53225, # Pterrordax Hatchling
        53232, # Voodoo Figurine
        106232,# Wyrmy Tunkins (Archaeology)
    )),
    ('Enchanting', (
        96403, # Enchanted Cauldron
        46898, # Enchanted Lantern
        96405, # Enchanted Pen
        96404, # Enchanted Torch
        50545, # Magic Lamp
    )),
    ('Engineering', (
        43916, # De-Weaponized Mechanical Companion
        80329, # Lifelike Mechanical Frostboar
        12419, # Lifelike Toad
         9657, # Lil' Smoky
        79410, # Mechanical Axebeak
        64899, # Mechanical Pandaren Dragonling
        88134, # Mechanical Scorpid
         2671, # Mechanical Squirrel
        43800, # Personal World Destroyer
         9656, # Pet Bombling
        70082, # Pierre
        71693, # Rascal-Bot
        15699, # Tranquil Mechanical Yeti
        106210,# Trigger (Engineering)
    )),
    ('Fishing', (
        26056, # Chuck
        126579,# Ghost Shark (fishing vendor)
        31575, # Giant Sewer Rat
        86445, # Land Shark
        18839, # Magical Crawdad
        24389, # Muckbreath
        84441, # Sea Calf
        55386, # Sea Pony
        26050, # Snarly
        109216,# Sting Ray Pup (fishing vendor)
        33226, # Strand Crawler
        70258, # Tiny Blue Carp
        70259, # Tiny Green Carp
        70257, # Tiny Red Carp
        70260, # Tiny White Carp
        24388, # Toothy
    )),
    ('Tailoring', (
        82464, # Elekk Plushie - Tailoring
        67230, # Imperial Moth - Tailoring
        67233, # Imperial Silkworm - Tailoring
    )),

    # Rare Mobs
    ('#Rare Mobs', ()),
    ('Other', (
        48982, # Tiny Shale Spider - Jadefang
    )),
    ('MoP', (
        64633, # Aqua Strider
        73533, # Ashleaf Spriteling
        73532, # Dandelion Frolicker
        73364, # Death Adder Hatchling
        70154, # Direhorn Runt
        64634, # Grinder
        73730, # Gu'chi Swarmling
        73359, # Gulp Froglet
        73738, # Jadefire Spirit
        73355, # Jademist Dancer
        50586, # Mr. Grubbs
        73357, # Ominous Flame
        73356, # Ruby Droplet
        73367, # Skunky Alemental
        70098, # Spawn of G'nathus
        73366, # Spineclaw Crab
        69778, # Sunreaver Micro-Sentry
        70451, # Zandalari Anklerender
        70452, # Zandalari Footslasher
        69796, # Zandalari Kneebiter
        70453, # Zandalari Toenibbler
    )),
    ('WoD', (
        86719, # Brilliant Spore - Spires of Arak
        88103, # Doom Bloom - Gorgrond
        98237, # Empowered Manafiend - Nagrand
        98238, # Empyreal Manafiend - Nagrand
        98236, # Energized Manafiend - Nagrand
        88490, # Eye of Observation - Talador
        91823, # Fel Pup - Tanaan
        86081, # Netherspawn - Nagrand
        88692, # Servant of Demidos - Shadowmoon
    )),
    ('Legion', (
        113136,# Benax (drop Suramar)
        98116, # Bleakwater Jelly (drop Helheim)
        98077, # Crispin (drop Highmountain)
        97179, # Eye of Inquisition (drop Suramar)
        99394, # Fetid Waveling (drop Val'sharah)
        111984,# Hungering Claw (drop? Kosumoth)
        108568,# Pygmy Owl (drop Val'sharah)
        99403, # Risen Saber Kitten (drop Val'sharah)
        97205, # Stormborne Whelpling (drop Stormheim)
    )),
    ('Legion - Argus', (
        128157, # Docile Skyfin (Fel-Spotted Egg, Argus rare drop)
        128158, # Fel-Afflicted Skyfin (Fel-Spotted Egg, Argus rare drop)
        128159, # Grasping Manifestation (Ataxon, Mac'Aree)
        128388, # Rebellious Imp (Drop, Mother Rosula)
        128396, # Uuna (Drop, The Many-Faced Devourer)
    )),

    # Reputation
    ('#Reputation', ()),
    ('TBC', (
        28470, # Nether Ray Fry - Sha'tari Skyguard
        25062, # Tiny Sporebat - Sporeggar
    )),
    ('WotLK', (
        32591, # Cobra Hatchling - The Oracles
        32595, # Pengu - The Kalu'ak
        32592, # Proto-Drake Whelp - The Oracles
        32589, # Tickbird Hatchling - The Oracles
        32590, # White Tickbird hatchling - The Oracles
    )),
    ('Cata', (
        48107, # Rustberg Gull - Baradin's Wardens
    )),
    ('MoP', (
        73732, # Harmonious Porcupette - Emperor Shaohao
        63370, # Red Cricket - Sho (MoP Farm)
        63559, # Tiny Goldfish - The Anglers
    )),
    ('WoD', (
        85281, # Albino River Calf - Steamwheedle Preservation Society
        96123, # Blazing Firehawk - Order of the Awakened
        85014, # Bone Wasp - Laughing Skull
        84885, # Draenei Micro Defender - Council of Exarchs
        83583, # Forest Sproutling - Steamwheedle Preservation Society
        87111, # Frostwolf Pup - Frostwolf Orcs
        96126, # Savage Cub - The Saberstalkers
        88452, # Sky Fry - Sha'tari Defense
        88401, # Son of Sethe - Arakkoa Outcasts
    )),
    ('Legion', (
        17254, # Ash'ana (rep Legion)
        106152,# Baby Elderhorn (rep)
        112728,# Court Scribe (rep)
        97174, # Extinguished Eye (rep Legion)
        97128, # Fledgling Warden Owl (rep Legion)
        121715,# Orphaned Felbat (Armies of Legionfall Paragon)
        128119,# Orphaned Marsuul (Argussian Reach - Honored)
        106181,# Sunborne Val'kyr (rep)
    )),

    # World Events
    ('#World Events', ()),
    ('Other', (
        68601, # Clock'em - Brawler's Guild
        34770, # Macabre Marionette - Day of the Dead
        55574, # Festival Lantern - Lunar Festival
        55571, # Lunar Lantern - Lunar Festival
        85773, # Mystical Spring Bouquet - Noblegarden
        33975, # Noblegarden Bunny - Noblegarden
        32791, # Spring Rabbit - Noblegarden
        85846, # Bush Chicken - Pilgrim's Bounty
        32818, # Plump Turkey - Pilgrim's Bounty
    )),
    ('Argent Tournament', (
        33239, # Argent Gruntling
        33238, # Argent Squire
        33205, # Ammen Vale Lashling
        33194, # Dun Morogh Cub
        33198, # Durotar Scorpion
        33200, # Elwynn Lamb
        33227, # Enchanted Broom
        33274, # Mechanopeep
        33219, # Mulgore Hatchling
        33810, # Sen'jin Fetish
        34724, # Shimmering Wyrmling
        33188, # Teldrassil Sproutling
        33197, # Tirisfal Batling
    )),
    ('Brewfest', (
        24753, # Pint-Sized Pink Pachyderm
        85994, # Stout Alemental
        22943, # Wolpertinger
    )),
    ('Children\'s Week', (
        33530, # Curious Oracle Hatchling
        33529, # Curious Wolvar Pup
        23258, # Egbert
        53048, # Legs
        16548, # Mr. Wiggles
        23266, # Peanut
        51635, # Scooter the Snail
        16547, # Speedy
        16549, # Whiskers the Rat
        23231, # Willy
    )),
    ('Darkmoon Faire', (
        93814, # Blorp
        67443, # Crow
        55187, # Darkmoon Balloon
        56031, # Darkmoon Cub
        67332, # Darkmoon Eye
        67329, # Darkmoon Glowfly
        67319, # Darkmoon Hatchling
        54491, # Darkmoon Monkey
        59358, # Darkmoon Rabbit
        55356, # Darkmoon Tonk
        54487, # Darkmoon Turtle
        55367, # Darkmoon Zeppelin
        93808, # Ghostshell Crab
        76873, # Hogs - That's Whack! achieve
        14878, # Jubling
        72160, # Moon Moon
        90345, # Race MiniZep - Big Rocketeer: Gold achieve
        55386, # Sea Pony
        85527, # Syd the Squid
         7549, # Tree Frog
         7550, # Wood Frog
    )),
    ('Hallow\'s End', (
        54128, # Creepy Crate
        86061, # Cursed Birman
        53884, # Feline Familiar
        97569, # Ghostly Maggot
        97568, # Ghostly Rat
        23909, # Sinister Squashling
        97324, # Spectral Spinner
        86067, # Widget the Departed
    )),
    ('Love is in the Air', (
        85710, # Lovebird Hatchling
        16085, # Peddlefeet
        38374, # Toxic Wasteling
    )),
    ('Midsummer', (
        85872, # Blazing Cindercrawler
        40198, # Frigid Frostling
        114543,# Igneous Flameling
        16701, # Spirit of Summer
    )),
    ('Winter Veil', (
        24968, # Clockwork Rocket Bot
        15698, # Father Winter's Helper
        128156, # Globe Yeti (Feast of Winter Veil)
        97229, # Grumpling
        55215, # Lumpy
        73741, # Rotten Little Helper
        15710, # Tiny Snowman
        15706, # Winter Reindeer
        15705, # Winter's Little Helper
    )),
    # End World Events

    # $$$$$
    ('#$$$$$', ()),
    ('BlizzCon', (
        15186, # Murky - BlizzCon 2005
        34694, # Grunty - BlizzCon 2009
        51122, # Deathy - BlizzCon 2010
        54438, # Murkablo - BlizzCon 2011
        74405, # Murkalot - BlizzCon 2013
        88805, # Grommloc - BlizzCon 2014
        85009, # Murkidan - BlizzCon 2015
        113983,# Knight-Captain Murky (Blizzcon 2016)
        113984,# Legionnaire Murky (Blizzcon 2016)
    )),
    ('CE', (
        11326, # Mini Diablo - Vanilla
        11325, # Panda Cub - Vanilla
        11327, # Zergling - Vanilla
        15358, # Lurky - TBC (EU)
        18381, # Netherwhelp - TBC
        28883, # Frosty - WotLK
        46896, # Lil' Deathwing - Cata
        63832, # Lucky Quilen Cub - MoP
        77137, # Dread Hatchling - WoD
        95841, # Nibbles - Legion
        56266, # Fetish Shaman - D3
        74413, # Treasure Goblin - D3:RoS
        42078, # Mini Thor - SC2:WoL
        66984, # Baneling - SC2:HotS
        71655, # Zeradar - SC2:LotV
    )),
    ('Store', (
        74402, # Alterac Brew-Pup
        88807, # Argi
        71488, # Blossoming Ancient
        85283, # Brightpaw
        53623, # Cenarion Hatchling
        68267, # Cinder Kitten
        36908, # Gryphon Hatchling
        53283, # Guardian Cub
        36979, # Lil' K.T.
        51600, # Lil' Ragnaros
        40703, # Lil' XT
        113527,# Mischief (Shop)
        51601, # Moonkin Hatchling (Alliance)
        51649, # Moonkin Hatchling (Horde)
        36911, # Pandaren Monk
        123650, # Shadow (Shop)
        58163, # Soul of the Aspects
        122033, # Twilight (Shop)
        36909, # Wind Rider Cub
    )),
    ('TCG', (
        23234, # Bananas
        25110, # Dragon Kite
        27914, # Ethereal Soul-Trader
        59020, # Eye of the Legion
        54730, # Gregarious Grell
        69208, # Gusting Grimoire
        17255, # Hippogryph Hatchling
        52343, # Landro's Lichling
        50468, # Landro's Lil' XT
        52344, # Nightsaber Cub
        54383, # Purple Puffer
        25109, # Rocket Chicken
        54745, # Sand Scarab
        36511, # Spectral Tiger Cub
        36482, # Tuskarr Kite
    )),
    # End $$$$$

    ('#Broken Isles Pet Battles', ()),
    ('Argus', (
        128175, # Antoran Bilescourge (Battle, Antoran Wastes, Argus)
        128163, # Antoran Bile Larva (Battle, Antoran Wastes, Argus)
        128164, # Bilescourge (Battle, Krokuun, Argus)
        128162, # Bile Larva (Battle, Krokuun, Argus)
        128166, # Flickering Argunite (Battle, Krokuun, Argus)
        128172, # Arcane Gorger (Battle, Mac'Aree, Argus)
        128174, # Felcrazed Wyrm (Battle, Mac'Aree, Argus)
        128173, # Pygmy Marsuul (Battle, Mac'Aree, Argus)
        128167, # Skyfin Juvenile (Battle, Mac'Aree, Argus)
        128171, # Void Shardling (Battle, Mac'Aree, Argus)
        128168, # Voidstalker Runt (Battle, Mac'Aree, Argus)
        128170, # Warpstalker Runt (Battle, Mac'Aree, Argus)
    )),
    ('Azsuna', (
        97323, # Felspider (Azsuna, Highmountain)
        97018, # Albatross Chick (Azsuna, Stormheim)
        110826,# Coastal Sandpiper (Azsuna, Suramar)
        98386, # Eldritch Manafiend (Azsuna)
        98385, # Erudite Manafiend (Azsuna)
        97076, # Fledgling Kingfeather (Azsuna)
        97078, # Fledgling Oliveback (Azsuna)
        97283, # Juvenile Scuttleback (Azsuna)
        97294, # Olivetail Hare (Azsuna)
        97542, # Slithering Brownscale (Azsuna, Stormheim, Val'sharah)
    )),

    ('Dalaran', (
        111158,# Blind Rat (Dalaran Sewers)
        98506, # Dust Bunny
        110666,# Young Mutant Warturtle
    )),

    ('Highmountain', (
        97741, # Black-Footed Fox Kit (Highmountain, Stormheim)
        97236, # Burrow Spiderling (Highmountain)
        98428, # Coralback Fiddler (Highmountain)
        88542, # Echo Batling (Highmountain)
        97323, # Felspider (Azsuna, Highmountain)
        98192, # Hog-Nosed Bat (Highmountain)
        97118, # Long-Eared Owl (Highmountain, Stormheim)
        97743, # Mist Fox Kit (Highmountain, Stormheim)
        98446, # Mudshell Conch (Highmountain)
        97126, # Northern Hawk Owl (Highmountain)
        98211, # Spiketail Beaver (Highmountain)
    )),
    ('Stormheim', (
        97018, # Albatross Chick (Azsuna, Stormheim)
        97741, # Black-Footed Fox Kit (Highmountain, Stormheim)
        97080, # Golden Eaglet (Stormheim)
        97118, # Long-Eared Owl (Highmountain, Stormheim)
        97743, # Mist Fox Kit (Highmountain, Stormheim)
        97840, # Rose Taipan (Stormheim)
        97542, # Slithering Brownscale (Azsuna, Stormheim, Val'sharah)
        111172,# Stormstruck Beaver (Stormheim)
        97952, # Tiny Apparition (Stormheim)
    )),
    ('Suramar', (
        110826,# Coastal Sandpiper (Azsuna, Suramar)
        99527, # Crystalline Broodling (Suramar)
        99528, # Thornclaw Broodling (Suramar)
        99513, # Vicious Broodling (Suramar)
    )),
    ('The Great Sea', (
        113440,# Squirky (The Great Sea)
    )),
    ("Val'sharah", (
        97555, # Auburn Ringtail (Val'sharah)
        110741,# Gleamhoof Fawn (Val'sharah)
        97511, # Shimmering Aquafly (Val'sharah)
        97542, # Slithering Brownscale (Azsuna, Stormheim, Val'sharah)
        97559, # Spring Strider (Val'sharah)
        97531, # Terror Larva (Val'sharah)
        97547, # Vale Flitter (Val'sharah)
    )),

    ('#Draenor Pet Battles', ()),

    ('Frostfire Ridge', (
        62886, # Fire-Proof Roach
        82715, # Frostfur Rat
        88480, # Frostshell Pincher
        85003, # Icespine Hatchling
        88474, # Ironclaw Scuttler
        61366, # Rat
        85253, # Twilight Wasp
    )),

    ('Gorgrond', (
        85257, # Amberbarb Wasp
        85389, # Axebeak Hatchling
        61752, # Brown Marmot
        61384, # Cockroach
        61438, # Gold Beetle
        85192, # Junglebeak
        63919, # Leopard Tree Frog
        88571, # Mudback Calf
        89194, # Parched Lizard
        63001, # Silkbead Snail
        85253, # Twilight Wasp
        85254, # Wood Wasp
    )),

    ('Nagrand', (
        61325, # Adder
        89198, # Leatherhide Runt
        83642, # Mud Jumper
        61141, # Prairie Dog
        61080, # Rabbit
        61366, # Rat
        61158, # Shore Crab
        61255, # Skunk
        61142, # Snake
        61081, # Squirrel
        61369, # Toad
    )),

    ('Shadowmoon Valley', (
        61320, # Forest Spiderling
        82045, # Moonshell Crab
        85005, # Mossbite Skitterer
        88417, # Royal Moth
        61327, # Spider
        88355, # Waterfly
        88466, # Zangar Crawler
    )),

    ('Spires of Arak', (
        61325, # Adder
        85255, # Bloodsting Wasp
        85798, # Golden Dawnfeather
        83642, # Mud Jumper
        61366, # Rat
        88356, # Sapphire Firefly
        63001, # Silkbead Snail
        61312, # Strand Crab
        88359, # Swamplighter Firefly
        85007, # Thicket Skitterer
        88355, # Waterfly
    )),

    ('Talador', (
        61420, # Ash Spiderling
        88385, # Brilliant Bloodfeather
        88413, # Crimsonwing Moth
        88572, # Flat-Tooth Calf
        88465, # Kelp Scuttler
        83642, # Mud Jumper
        61757, # Red-Tailed Chipmunk
        88417, # Royal Moth
        62953, # Sea Gull
        88576, # Shadow Sporebat
        63001, # Silkbead Snail
        88355, # Waterfly
    )),

    ('Tanaan Jungle', (
        85388, # Bloodbeak
        88422, # Cerulean Moth
        62621, # Fel Flame
        88473, # Fen Crab
        62185, # Horny Toad
        61753, # Maggot
        61318, # Tree Python
        88357, # Violet Firefly
        88355, # Waterfly
    )),
    # End Draenor

    # Pandaria
    ('#Pandaria Pet Battles', ()),

    ('Dread Wastes', (
        65187, # Amber Moth
        64242, # Clouded Hedgehog
        63548, # Crunchy Scorpion
        65203, # Emperor Crab
        64352, # Rapana Whelk
        64238, # Resilient Roach
        64804, # Silent Hedgehog
    )),

    ('Isle of Thunder', (
        69818, # Elder Python
        69794, # Electrified Razortooth
        69819, # Swamp Croaker
        69648, # Thundertail Flapper
    )),

    ('Krasarang Wilds', (
        63288, # Amethyst Spiderling
        62994, # Emerald Turtle
        65054, # Feverbite
        63304, # Jungle Grub
        65124, # Luyu Moth
        63094, # Malayan Quillrat
        65185, # Mei Li Sparkler
        63291, # Savory Beetle
        62953, # Sea Gull
        63057, # Sifang Otter
        63358, # Sifang Otter Pup
        63293, # Spiny Terrapin
        68506, # Sumprush Rodent
        67022, # Wanderer's Festival Hatchling
    )),

    ('Kun-Lai Summit', (
        63550, # Alpine Foxling
        63551, # Alpine Foxling Kit
        63850, # Effervescent Glowfly
        68846, # Kun-Lai Runt
        63547, # Plains Monitor
        59702, # Prairie Mouse
        64248, # Summit Kid
        63585, # Szechuan Chicken
        63557, # Tolai Hare
        63558, # Tolai Hare Pup
        63555, # Zooey Snake
    )),

    ('The Jade Forest', (
        63062, # Bandicoon
        63064, # Bandicoon Kit
        62992, # Bucktooth Flapper
        62991, # Coral Adder
        62994, # Emerald Turtle
        63002, # Garden Frog
        65215, # Garden Moth
        63838, # Gilded Moth
        63004, # Grove Viper
        63715, # Jumping Spider
        62997, # Jungle Darter
        63919, # Leopard Tree Frog
        63094, # Malayan Quillrat
        63003, # Masked Tanuki
        63716, # Masked Tanuki Pup
        62998, # Mirror Strider
        63006, # Sandy Petrel
        65216, # Shrine Fly
        63057, # Sifang Otter
        63358, # Sifang Otter Pup
        63001, # Silkbead Snail
        63293, # Spiny Terrapin
        63005, # Spirebound Crab
        62999, # Temple Snake
        65321, # Wild Crimson Hatchling
        65324, # Wild Golden Hatchling
        65323, # Wild Jade Hatchling
    )),

    ('The Veiled Stair', (
        63062, # Bandicoon
        63094, # Malayan Quillrat
        63095, # Malayan Quillrat Pup
    )),

    ('Timeless Isle', (
        73542, # Ashwing Moth
        73543, # Flamering Moth
        73368, # Skywisp Moth
    )),

    ('Townlong Steppes', (
        63550, # Alpine Foxling
        63551, # Alpine Foxling Kit
        65187, # Amber Moth
        64242, # Clouded Hedgehog
        63548, # Crunchy Scorpion
        63549, # Grassland Hopper
        63953, # Kuitan Mongoose
        65190, # Mongoose
        63954, # Mongoose Pup
        64804, # Silent Hedgehog
        63557, # Tolai Hare
        63957, # Yakrat
    )),

    ('Vale of Eternal Blossoms', (
        63847, # Dancing Water Skimmer
        63850, # Effervescent Glowfly
        61088, # Eternal Strider
        63838, # Gilded Moth
        63841, # Golden Civet
        63842, # Golden Civet Kitten
        63849, # Yellow-Bellied Bullfrog
    )),

    ('Valley of the Four Winds', (
        63062, # Bandicoon
        63064, # Bandicoon Kit
        62992, # Bucktooth Flapper
        62994, # Emerald Turtle
        63094, # Malayan Quillrat
        63095, # Malayan Quillrat Pup
        63096, # Marsh Fiddler
        64246, # Shy Bandicoon
        63057, # Sifang Otter
        63358, # Sifang Otter Pup
        63060, # Softshell Snapling
    )),
    # End Pandaria

    # Northrend
    ('#Northrend Pet Battles', ()),

    ('Borean Tundra', (
        62693, # Arctic Hare
        62695, # Borean Marmot
        68845, # Nexus Whelpling
        62697, # Oily Slimeling
        61158, # Shore Crab
        62835, # Tundra Penguin
        71163, # Unborn Val'kyr
    )),

    ('Crystalsong Forest', (
        61143, # Mouse
        61080, # Rabbit
        61366, # Rat
        61081, # Squirrel
        71163, # Unborn Val'kyr
    )),

    ('Dragonblight', (
        62693, # Arctic Hare
        62852, # Dragonbone Hatchling
        62835, # Tundra Penguin
        71163, # Unborn Val'kyr
    )),

    ('Grizzly Hills', (
        61165, # Fawn
        62818, # Grizzly Squirrel
        62819, # Imperial Eagle Chick
        61677, # Mountain Skunk
        71163, # Unborn Val'kyr
    )),

    ('Howling Fjord', (
        62664, # Chicken
        62640, # Devouring Maggot
        62641, # Fjord Rat
        62669, # Fjord Worg Pup
        62818, # Grizzly Squirrel
        61753, # Maggot
        61677, # Mountain Skunk
        61080, # Rabbit
        61366, # Rat
        61169, # Roach
        61158, # Shore Crab
        61255, # Skunk
        61142, # Snake
        61327, # Spider
        61081, # Squirrel
        61369, # Toad
        62648, # Turkey
        71163, # Unborn Val'kyr
    )),

    ('Icecrown', (
        61384, # Cockroach
        62854, # Scourged Whelpling
        71163, # Unborn Val'kyr
    )),

    ('Sholazar Basin', (
        62815, # Biletoad
        68850, # Emerald Proto-Whelp
        61142, # Snake
        61081, # Squirrel
        62816, # Stunted Shardhorn
        71163, # Unborn Val'kyr
    )),

    ('The Storm Peaks', (
        62864, # Arctic Fox Kit
        62693, # Arctic Hare
        61677, # Mountain Skunk
        61327, # Spider
        71163, # Unborn Val'kyr
    )),

    ('Zul\'Drak', (
        62693, # Arctic Hare
        61368, # Huge Toad
        61142, # Snake
        61327, # Spider
        71163, # Unborn Val'kyr
        62820, # Water Waveling
    )),
    # End Northrend

    # Outland
    ('#Outland Pet Battles', ()),

    ('Blade\'s Edge Mountains', (
        61752, # Brown Marmot
        68841, # Cogblade Raptor
        61080, # Rabbit
        62184, # Rock Viper
        62628, # Scalded Basilisk Hatchling
        61326, # Scorpid
        62638, # Skittering Cavern Crawler
    )),

    ('Hellfire Peninsula', (
        61325, # Adder
        61326, # Scorpid
    )),

    ('Nagrand', (
        61325, # Adder
        62620, # Clefthoof Runt
        61141, # Prairie Dog
        61080, # Rabbit
        61366, # Rat
        61142, # Snake
        61081, # Squirrel
        61369, # Toad
    )),

    ('Netherstorm', (
        62019, # Cat
        62627, # Fledgling Nether Ray
        61143, # Mouse
        62625, # Nether Roach
    )),

    ('Shadowmoon Valley', (
        61385, # Ash Viper
        62621, # Fel Flame
        62314, # Tainted Cockroach
        62583, # Warpstalker Hatchling
    )),

    ('Shattrath City', (
        61366, # Rat
    )),

    ('Terokkar Forest', (
        62555, # Flayer Youngling
        61366, # Rat
        61255, # Skunk
        61142, # Snake
        61081, # Squirrel
        61440, # Stripe-Tailed Scorpid
        62583, # Warpstalker Hatchling
    )),

    ('Zangarmarsh', (
        61071, # Small Frog
        61142, # Snake
        62564, # Sporeling Sprout
    )),
    # End Outland

    # The Maelstrom
    ('#The Maelstrom Pet Battles', ()),

    ('Darkmoon Island', (
        67443, # Crow
        67329, # Darkmoon Glowfly
        62953, # Sea Gull
    )),

    ('Deepholm', (
        62182, # Amethyst Shale Hatchling
        62927, # Crimson Geode
        62922, # Crimson Shale Hatchling
        62925, # Crystal Beetle
        62924, # Deepholm Cockroach
        62915, # Emerald Shale Hatchling
        62916, # Fungal Moth
        62921, # Stowaway Rat
        62181, # Topaz Shale Hatchling
        62118, # Twilight Beetle
        62117, # Twilight Spider
    )),
    # End The Maelstrom

    # Eastern Kingdoms
    ('#Eastern Kingdoms Pet Battles', ()),

    ('Arathi Highlands', (
        62019, # Cat
        61704, # Grasslands Cottontail
        61751, # Hare
        61141, # Prairie Dog
        61366, # Rat
        61071, # Small Frog
        61703, # Tiny Twister
    )),

    ('Badlands', (
        61319, # Beetle
        61257, # Black Rat
        61438, # Gold Beetle
        61443, # King Snake
        61439, # Rattlesnake
        61441, # Spiky Lizard
        61440, # Stripe-Tailed Scorpid
    )),

    ('Blasted Lands', (
        61325, # Adder
        61328, # Fire Beetle
        61326, # Scorpid
        61329, # Scorpling
        61327, # Spider
    )),

    ('Burning Steppes', (
        61385, # Ash Viper
        61384, # Cockroach
        61328, # Fire Beetle
        61386, # Lava Beetle
        61383, # Lava Crab
        61326, # Scorpid
    )),

    ('Deadwind Pass', (
        68819, # Arcane Eye
        61375, # Restless Shadeling
    )),

    ('Dun Morogh', (
        61325, # Adder
        61690, # Alpine Hare
        68838, # Fluxfire Feline
        61691, # Irradiated Roach
        61753, # Maggot
        61080, # Rabbit
        61366, # Rat
        61169, # Roach
        61689, # Snow Cub
        61327, # Spider
        61312, # Strand Crab
        61369, # Toad
    )),

    ('Duskwood', (
        61257, # Black Rat
        62664, # Chicken
        61253, # Dusk Spiderling
        61143, # Mouse
        61080, # Rabbit
        61258, # Rat Snake
        61169, # Roach
        61255, # Skunk
        61071, # Small Frog
        61081, # Squirrel
        61259, # Widow Spiderling
    )),

    ('Eastern Plaguelands', (
        61829, # Bat
        61257, # Black Rat
        61830, # Festering Maggot
        61827, # Infected Fawn
        61828, # Infected Squirrel
        61326, # Scorpid
        61327, # Spider
    )),

    ('Elwynn Forest', (
        60649, # Black Lamb
        62019, # Cat
        62664, # Chicken
        61165, # Fawn
        61080, # Rabbit
        61071, # Small Frog
        61327, # Spider
        61081, # Squirrel
        62954, # Stormwind Rat
    )),

    ('Eversong Woods', (
        62019, # Cat
        61080, # Rabbit
        62020, # Ruby Sapling
        61071, # Small Frog
        61142, # Snake
        61369, # Toad
    )),

    ('Ghostlands', (
        62022, # Larva
        61753, # Maggot
        61366, # Rat
        61071, # Small Frog
        61142, # Snake
        61327, # Spider
        62034, # Spirit Crab
        61369, # Toad
    )),

    ('Gilneas', (
        61327, # Spider
        61369, # Toad
    )),

    ('Gilneas City', (
        61327, # Spider
    )),

    ('Hillsbrad Foothills', (
        62664, # Chicken
        61368, # Huge Toad
        61758, # Infested Bear Cub
        68806, # Lofty Libram
        61753, # Maggot
        61080, # Rabbit
        61366, # Rat
        61757, # Red-Tailed Chipmunk
        61755, # Snowshoe Hare
        61327, # Spider
        61081, # Squirrel
        61369, # Toad
    )),

    ('Ironforge', (
        61317, # Long-tailed Mole
    )),

    ('Loch Modan', (
        61459, # Little Black Ram
        61366, # Rat
        61169, # Roach
        61071, # Small Frog
        61142, # Snake
        61689, # Snow Cub
        61081, # Squirrel
    )),

    ('New Tinkertown', (
        61690, # Alpine Hare
        68838, # Fluxfire Feline
        61691, # Irradiated Roach
        61689, # Snow Cub
    )),

    ('Northern Stranglethorn', (
        61319, # Beetle
        61314, # Crimson Moth
        61320, # Forest Spiderling
        61321, # Lizard Hatchling
        61317, # Long-tailed Mole
        61313, # Parrot
        61322, # Polly
        61169, # Roach
        61312, # Strand Crab
        61318, # Tree Python
    )),

    ('Redridge Mountains', (
        62664, # Chicken
        61171, # Fledgling Buzzard
        61167, # Mountain Cottontail
        61080, # Rabbit
        61168, # Redridge Rat
        61169, # Roach
    )),

    ('Searing Gorge', (
        61420, # Ash Spiderling
        61328, # Fire Beetle
        61443, # King Snake
        61383, # Lava Crab
        61425, # Molten Hatchling
    )),

    ('Silvermoon City', (
        62019, # Cat
        61143, # Mouse
        61080, # Rabbit
    )),

    ('Silverpine Forest', (
        61890, # Blighted Squirrel
        61827, # Infected Fawn
        61828, # Infected Squirrel
        61080, # Rabbit
        61366, # Rat
        61757, # Red-Tailed Chipmunk
        61142, # Snake
        61081, # Squirrel
        61369, # Toad
    )),

    ('Stormwind City', (
        61080, # Rabbit
        61366, # Rat
        61081, # Squirrel
        62954, # Stormwind Rat
        61081, # Squirrel
    )),

    ('Sunstrider Isle', (
        62019, # Cat
    )),

    ('Swamp of Sorrows', (
        61368, # Huge Toad
        61372, # Moccasin
        61313, # Parrot
        61366, # Rat
        61327, # Spider
        61312, # Strand Crab
        61370, # Swamp Moth
        61369, # Toad
        61367, # Water Snake
    )),

    ('The Cape of Stranglethorn', (
        61324, # Baby Ape
        61319, # Beetle
        61314, # Crimson Moth
        61320, # Forest Spiderling
        61321, # Lizard Hatchling
        61317, # Long-tailed Mole
        61313, # Parrot
        61366, # Rat
        61169, # Roach
        61312, # Strand Crab
        61318, # Tree Python
        61323, # Wharf Rat
    )),

    ('The Hinterlands', (
        61752, # Brown Marmot
        61384, # Cockroach
        61751, # Hare
        61718, # Jade Oozeling
        61753, # Maggot
        61366, # Rat
        61327, # Spider
    )),

    ('Tirisfal Glades', (
        61829, # Bat
        62664, # Chicken
        61905, # Lost of Lordaeron
        61753, # Maggot
        61080, # Rabbit
        61366, # Rat
        61169, # Roach
        61327, # Spider
    )),

    ('Twilight Highlands', (
        61257, # Black Rat
        61384, # Cockroach
        62818, # Grizzly Squirrel
        62905, # Highlands Mouse
        62907, # Highlands Skunk
        62906, # Highlands Turkey
        61368, # Huge Toad
        61439, # Rattlesnake
        61326, # Scorpid
        61158, # Shore Crab
        61369, # Toad
        62914, # Twilight Fiendling
        62117, # Twilight Spider
        61367, # Water Snake
        62904, # Yellow-Bellied Marmot
        62900, # Wildhammer Gryphon Hatchling
    )),

    ('Undercity', (
        61829, # Bat
        61905, # Lost of Lordaeron
        61169, # Roach
        61889, # Undercity Rat
    )),

    ('Western Plaguelands', (
        61257, # Black Rat
        61826, # Blighthawk
        61080, # Rabbit
        61081, # Squirrel
    )),

    ('Westfall', (
        62664, # Chicken
        61143, # Mouse
        61141, # Prairie Dog
        61080, # Rabbit
        61158, # Shore Crab
        61071, # Small Frog
        61142, # Snake
        61081, # Squirrel
        61160, # Tiny Harvester
    )),

    ('Wetlands', (
        61257, # Black Rat
        61384, # Cockroach
        62905, # Highlands Mouse
        61677, # Mountain Skunk
        61143, # Mouse
        61080, # Rabbit
        61081, # Squirrel
        61686, # Tiny Bog Beast
        61369, # Toad
        61367, # Water Snake
    )),
    # End Eastern Kingdoms

    # Kalimdor
    ('#Kalimdor Pet Battles', ()),

    ('Ahn\'Qiraj: The Fallen Kingdom', (
        62524, # Scarab Hatchling
        62523, # Sidewinder
    )),

    ('Ammen Vale', (
        62050, # Grey Moth
    )),

    ('Ashenvale', (
        61319, # Beetle
        62177, # Forest Moth
        62312, # Frog
        61753, # Maggot
        61080, # Rabbit
        61366, # Rat
        61169, # Roach
        62313, # Rusty Snail
        61081, # Squirrel
        61369, # Toad
    )),

    ('Azshara', (
        61080, # Rabbit
        62120, # Rabid Nut Varmint 5000
        61366, # Rat
        61169, # Roach
        62119, # Robo-Chick
        61158, # Shore Crab
        61255, # Skunk
        61327, # Spider
        61081, # Squirrel
        62121, # Turquoise Turtle
        62118, # Twilight Beetle
        62117, # Twilight Spider
    )),

    ('Azuremyst Isle', (
        62050, # Grey Moth
        61080, # Rabbit
        61255, # Skunk
        61081, # Squirrel
    )),

    ('Bloodmyst Isle', (
        62664, # Chicken
        61827, # Infected Fawn
        61828, # Infected Squirrel
        61366, # Rat
        62051, # Ravager Hatchling
        61255, # Skunk
    )),

    ('Darkshore', (
        62250, # Darkshore Cub
        61080, # Rabbit
        61366, # Rat
        62246, # Shimmershell Snail
        61081, # Squirrel
    )),

    ('Darnassus', (
        62178, # Elfin Rabbit
        62177, # Forest Moth
        61757, # Red-Tailed Chipmunk
        61071, # Small Frog
    )),

    ('Desolace', (
        62182, # Amethyst Shale Hatchling
        62186, # Desert Spider
        62178, # Elfin Rabbit
        62177, # Forest Moth
        62185, # Horny Toad
        61366, # Rat
        61757, # Red-Tailed Chipmunk
        61169, # Roach
        62184, # Rock Viper
        62187, # Stone Armadillo
        62181, # Topaz Shale Hatchling
    )),

    ('Durotar', (
        61325, # Adder
        61384, # Cockroach
        62116, # Creepy Crawly
        62115, # Dung Beetle
        61751, # Hare
        62114, # Spiny Lizard
        61369, # Toad
        61367, # Water Snake
    )),

    ('Dustwallow Marsh', (
        61257, # Black Rat
        61143, # Mouse
        61142, # Snake
        62201, # Spawn of Onyxia
        61327, # Spider
        61369, # Toad
    )),

    ('Felwood', (
        62317, # Minfernal
        62314, # Tainted Cockroach
        62315, # Tainted Moth
        62316, # Tainted Rat
        61369, # Toad
    )),

    ('Feralas', (
        62395, # Nether Faerie Dragon
        61080, # Rabbit
        61142, # Snake
        61081, # Squirrel
        68805, # Stunted Yeti
    )),

    ('Moonglade', (
        62177, # Forest Moth
        61080, # Rabbit
        62373, # Silky Moth
        61081, # Squirrel
    )),

    ('Mount Hyjal', (
        62189, # Alpine Chipmunk
        62364, # Ash Lizard
        61829, # Bat
        62885, # Carrion Rat
        62887, # Death's Head Cockroach
        62178, # Elfin Rabbit
        61328, # Fire Beetle
        62886, # Fire-Proof Roach
        62177, # Forest Moth
        62884, # Grotto Vole
        62888, # Nordrassil Wisp
        61080, # Rabbit
        62184, # Rock Viper
        62373, # Silky Moth
        61081, # Squirrel
        62118, # Twilight Beetle
    )),

    ('Mulgore', (
        62176, # Gazelle Fawn
        61143, # Mouse
        61141, # Prairie Dog
        61080, # Rabbit
    )),

    ('Northern Barrens', (
        61325, # Adder
        62129, # Cheetah Cub
        62127, # Emerald Boa
        68804, # Harpy Youngling
        61141, # Prairie Dog
        61071, # Small Frog
    )),

    ('Orgrimmar', (
        62115, # Dung Beetle
        62119, # Robo-Chick
        61327, # Spider
        62114, # Spiny Lizard
        61369, # Toad
        61367, # Water Snake
    )),

    ('Silithus', (
        61319, # Beetle
        62186, # Desert Spider
        62526, # Qiraji Guardling
        62184, # Rock Viper
        62524, # Scarab Hatchling
        61326, # Scorpid
        62523, # Sidewinder
        61441, # Spiky Lizard
    )),

    ('Southern Barrens', (
        61325, # Adder
        62129, # Cheetah Cub
        62127, # Emerald Boa
        62130, # Giraffe Calf
        61141, # Prairie Dog
        61071, # Small Frog
    )),

    ('Stonetalon Mountains', (
        62189, # Alpine Chipmunk
        62190, # Coral Snake
        61677, # Mountain Skunk
        61080, # Rabbit
        62120, # Rabid Nut Varmint 5000
        61366, # Rat
        61169, # Roach
        61327, # Spider
        62191, # Venomspitter Hatchling
    )),

    ('Tanaris', (
        62186, # Desert Spider
        61438, # Gold Beetle
        68820, # Infinite Whelpling
        61439, # Rattlesnake
        62257, # Sand Kitten
        62953, # Sea Gull
        62258, # Silithid Hatchling
        62256, # Stinkbug
        61440, # Stripe-Tailed Scorpid
    )),

    ('Teldrassil', (
        62242, # Crested Owl
        62178, # Elfin Rabbit
        61165, # Fawn
        62177, # Forest Moth
        61080, # Rabbit
        61757, # Red-Tailed Chipmunk
        61071, # Small Frog
        61327, # Spider
        61369, # Toad
    )),

    ('The Exodar', (
        62050, # Grey Moth
        61143, # Mouse
    )),

    ('Thousand Needles', (
        61169, # Roach
        61326, # Scorpid
        62255, # Twilight Iguana
    )),

    ('Thunder Bluff', (
        61141, # Prairie Dog
    )),

    ('Uldum', (
        62186, # Desert Spider
        62115, # Dung Beetle
        62127, # Emerald Boa
        62894, # Horned Lizard
        62896, # Leopard Scorpid
        62893, # Locust
        62892, # Mac Frog
        62895, # Oasis Moth
        62523, # Sidewinder
        62899, # Tol'vir Scarab
    )),

    ('Un\'Goro Crater', (
        62364, # Ash Lizard
        61319, # Beetle
        61384, # Cockroach
        62375, # Diemetradon Hatchling
        62127, # Emerald Boa
        61328, # Fire Beetle
        61317, # Long-tailed Mole
        61313, # Parrot
        62373, # Silky Moth
        62370, # Spotted Bell Frog
        61318, # Tree Python
    )),

    ('Winterspring', (
        62189, # Alpine Chipmunk
        61690, # Alpine Hare
        68839, # Anodized Robo Cub
        62435, # Crystal Spider
        62120, # Rabid Nut Varmint 5000
        62119, # Robo-Chick
         7554, # Snowy Owl
        61327, # Spider
    )),
    # End Kalimdor
)

all_pets = set()
for set_name, pet_ids in pets:
    all_pets.update(pet_ids)

# ---------------------------------------------------------------------------
