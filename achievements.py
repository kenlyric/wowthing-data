# ---------------------------------------------------------------------------

achievement_reps = {
    # Alliance AB
    711: 509,
    # Alliance AV
    709: 730,
    # Alliance WSG
    713: 890,
    # Horde AB
    710: 510,
    # Horde AV
    708: 729,
    # Horde WSG
    712: 889,

    # Argent Dawn
    946: 529,
    # Brood of Nozdormu
    956: 910,
    # Hydraxian Waterlords
    955: 749,
    # Timbermaw Hold
    944: 576,

    # Ashtongue Deathsworn
    958: 1012,
    # Kurenai
    899: 978,
    # Netherwing
    898: 1015,
    # Ogri'la
    896: 1038,
    # Sha'tari Skyguard
    894: 1031,
    # Shattered Sun Offensive
    897: 1077,
    # Sporeggar
    900: 970,
    # The Consortium
    902: 933,
    # The Mag'har
    901: 941,
    # The Scale of the Sands
    959: 990,
    # The Violet Eye
    960: 967,

    # Alliance Vanguard
    1012: 1037,
    # Argent Crusade
    947: 1106,
    # Frenzyheart Tribe
    950: 1104,
    # Horde Expedition
    1011: 1052,
    # Knights of the Ebon Blade
    1009: 1098,
    # The Ashen Verdict
    4598: 1156,
    # The Kalu'ak
    949: 1073,
    # Kirin Tor
    1008: 1090,
    # The Oracles
    951: 1105,
    # The Wyrmrest Accord
    1007: 1091,

    # Avengers of Hyjal
    5827: 1204,
    # Baradin's Wardens
    5375: 1177,
    # Dragonmaw Clan
    4886: 1172,
    # Guardians of Hyjal
    4882: 1158,
    # Hellscream's Reach
    5376: 1178,
    # Ramkahen
    4884: 1173,
    # The Earthen Ring
    4881: 1135,
    # Therazane
    4883: 1171,
    # Wildhammer Clan
    4885: 1174,

    # Dominance Offensive
    8206: 1375,
    # Emperor Shaohao
    8715: 1492,
    # Golden Lotus
    6546: 1269,
    # Kirin Tor Offensive
    8208: 1387,
    # Operation: Shieldwall
    8205: 1376,
    # Order of the Cloud Serpent
    6550: 1271,
    # Shado-Pan
    6366: 1270,
    # Shado-Pan Assault
    8210: 1435,
    # Sunreaver Onslaught
    8209: 1388,
    # The Anglers
    6547: 1302,
    # The August Celestials
    6543: 1341,
    # The Klaxxi
    6545: 1337,
    # The Lorewalkers
    6548: 1345,
    # The Tillers
    6544: 1272,
    # Nat Pagle
    7274: 1358,

    # Arakkoa Outcasts
    9469: 1515,
    # Council of Exarchs
    9470: 1731,
    # Frostwolf Orcs
    9471: 1445,
    # Laughing Skull Orcs
    9475: 1708,
    # Sha'tari Defense
    9476: 1710,
    # Steamwheedle Preservation Society
    9472: 1711,
    # Vol'jin's Spear
    9473: 1681,
    # Vol'jin's Spear (Ashran)
    9215: 1681,
    # Wrynn's Vanguard
    9474: 1682,
    # Wrynn's Vanguard (Ashran)
    9214: 1682,

    # The Nightfallen
    10778: 1859,
}

# ---------------------------------------------------------------------------
