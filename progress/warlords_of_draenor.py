progress_warlords = [
    ['Zone Quests', [
        dict(
            alliance_name='Shadowmoon Valley Storylines',
            alliance_icon='achievement_zone_newshadowmoonvalley',
            horde_name='Frostfire Ridge Storylines',
            horde_icon='achievement_zone_frostfire',
            level=90,
            type='quest',
            alliance_things=[
                [[34692], 'Establishing a Foothold'],
                [[34019], 'Shadows Awaken'],
                [[34054], 'The Dark Side of the Moon'],
                [[33256], 'The Light Prevails'],
                [[33271], 'Gloomshade Grove'],
                [[34792], 'The Pursuit of Justice'],
                [[35015], 'Purifying the Gene Pool'],
            ],
            horde_things=[
                [[34775], 'Foothold in a Savage Land'],
                [[33527], 'Siege of Bladespire Citadel'],
                [[33473], "Defense of Wor'gol"],
                [[32796], "Ga'nar's Vengeance"],
                [[33828], "Thunder's Fall"],
                [[34124], 'The Battle of Thunder Pass'],
            ],
        ),
        dict(
            name='Gorgrond Storylines',
            icon='achievement_zone_gorgrond',
            level=92,
            type='quest',
            things=[
            ],
        ),
        dict(
            name='Talador Storylines',
            icon='achievement_zone_talador',
            level=94,
            type='quest',
            things=[
            ],
        ),
        dict(
            name='Spires of Arak Storylines',
            icon='achievement_zone_spiresofarak',
            level=96,
            type='quest',
            things=[
            ],
        ),
        dict(
            name='Nagrand Storylines',
            icon='achievement_zone_nagrand_02',
            level=98,
            type='quest',
            things=[
            ],
        ),
    ]],
]
